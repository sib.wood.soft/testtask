// Fill out your copyright notice in the Description page of Project Settings.

#include "Prehensile.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SceneComponent.h"
#include "Components/SpotLightComponent.h"

// Sets default values
APrehensile::APrehensile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetSimulatePhysics(true);
	Mesh->SetNotifyRigidBodyCollision(true);
	SetRootComponent(Mesh);

	SphereComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	SphereComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);

	LightComponent = CreateDefaultSubobject<UPointLightComponent>("LightComponent");
	LightComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void APrehensile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APrehensile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APrehensile::OnGrab()
{
	USceneComponent * Root = GetRootComponent();
	if (Root->IsA(UStaticMeshComponent::StaticClass()))
	{
		static_cast<UStaticMeshComponent*>(Root)->SetSimulatePhysics(false);
	}
}

void APrehensile::OnDrop()
{
	USceneComponent * Root = GetRootComponent();

	if (Root->IsA(UStaticMeshComponent::StaticClass()))
	{
		static_cast<UStaticMeshComponent*>(Root)->SetSimulatePhysics(true);
	}
}
