// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Components/SpotLightComponent.h"
#include "Prehensile.generated.h"

UCLASS()
class TESTTASK_API APrehensile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APrehensile();
	virtual void Tick(float DeltaTime) override;
	virtual void OnGrab();
	virtual void OnDrop();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, Category = Mesh)
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, Category = Mesh)
	USphereComponent* SphereComponent;
	UPROPERTY(EditAnywhere, Category = Mesh)
	UPointLightComponent* LightComponent;
	UPROPERTY(EditAnywhere,Category=Gameplay)
	int ElevationLevel;
};
