// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/ChildActorComponent.h"
#include "Camera/CameraComponent.h"
#include "./Hands/PlayerHandAnim.h"
#include "./Hands/HandActor.h"
#include "VRPlayerPawn.generated.h"


UCLASS()
class TESTTASK_API AVRPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void GrabLeft();
	void DropLeft();
	void GrabRight();
	void DropRight();
	void PointedLeft();
	void UnpointLeft();
	void PointedRight();
	void UnpointRight();
	void ThumbUpLeft();
	void ThumbDownLeft();
	void ThumbUpRight();
	void ThumbDownRight();
	void IncreaseLevel();
	void DecreaseLevel();

protected:
	virtual void BeginPlay() override;

public:
	// Sets default values for this pawn's properties
	AVRPlayerPawn();
	UPROPERTY(VisibleDefaultsOnly)
	UCapsuleComponent* CollisionComponent;
	UPROPERTY(VisibleDefaultsOnly)
	USceneComponent* VRComponent;
	UPROPERTY(VisibleDefaultsOnly)
	UCameraComponent* CameraComponent;
	UPROPERTY(VisibleDefaultsOnly)
	UChildActorComponent* InventoryActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hands)
	TSubclassOf<AHandActor> LeftHand;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hands)
	TSubclassOf<AHandActor> RightHand;

protected:
	AHandActor* LeftHandActor;
	AHandActor* RightHandActor;
	int CurrentElevationLevel = 1;

private:
	void SetIntensityForEqLevelActors(TArray<AActor*> Actors, float Intensity);
	void ChangeElevationLevel(int NewLevel);
};
