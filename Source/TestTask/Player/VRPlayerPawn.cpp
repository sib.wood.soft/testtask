// Fill out your copyright notice in the Description page of Project Settings.


#include "VRPlayerPawn.h"
#include "Kismet/GameplayStatics.h"
#include "Components/MeshComponent.h"
#include "TestTask/Interfaces/Prehensile.h"

// Sets default values
AVRPlayerPawn::AVRPlayerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("SphereComponent"));
	CollisionComponent->AttachToComponent(RootComponent,
	                                      FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
	RootComponent = CollisionComponent;

	VRComponent = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	VRComponent->AttachToComponent(CollisionComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->AttachToComponent(VRComponent, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true));
}

// Called when the game starts or when spawned
void AVRPlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;

	if (LeftHand)
	{
		LeftHandActor = GetWorld()->SpawnActor<AHandActor>(LeftHand, FVector(0, 0, 0), FRotator(0, 0, 0), SpawnParams);
		LeftHandActor->AttachToComponent(VRComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Left hand is not set"));
	}

	if (RightHand)
	{
		RightHandActor = GetWorld()->SpawnActor<
			AHandActor>(RightHand, FVector(0, 0, 0), FRotator(0, 0, 0), SpawnParams);
		RightHandActor->AttachToComponent(VRComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Left hand is not set"));
	}
	ChangeElevationLevel(1);
}

// Called every frame
void AVRPlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AVRPlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAction("GrabLeft", IE_Pressed, this, &AVRPlayerPawn::GrabLeft);
	PlayerInputComponent->BindAction("GrabLeft", IE_Released, this, &AVRPlayerPawn::DropLeft);
	PlayerInputComponent->BindAction("GrabRight", IE_Pressed, this, &AVRPlayerPawn::GrabRight);
	PlayerInputComponent->BindAction("GrabRight", IE_Released, this, &AVRPlayerPawn::DropRight);
	PlayerInputComponent->BindAction("PointingLeft", IE_Pressed, this, &AVRPlayerPawn::PointedLeft);
	PlayerInputComponent->BindAction("PointingLeft", IE_Released, this, &AVRPlayerPawn::UnpointLeft);
	PlayerInputComponent->BindAction("PointingRight", IE_Pressed, this, &AVRPlayerPawn::PointedRight);
	PlayerInputComponent->BindAction("PointingRight", IE_Released, this, &AVRPlayerPawn::UnpointRight);
	PlayerInputComponent->BindAction("ThumbLeft", IE_Pressed, this, &AVRPlayerPawn::ThumbUpLeft);
	PlayerInputComponent->BindAction("ThumbLeft", IE_Released, this, &AVRPlayerPawn::ThumbDownLeft);
	PlayerInputComponent->BindAction("ThumbRight", IE_Pressed, this, &AVRPlayerPawn::ThumbUpRight);
	PlayerInputComponent->BindAction("ThumbRight", IE_Released, this, &AVRPlayerPawn::ThumbDownRight);
	PlayerInputComponent->BindAction("IncreaseLevel", IE_Pressed, this, &AVRPlayerPawn::IncreaseLevel);
	PlayerInputComponent->BindAction("DecreaseLevel", IE_Released, this, &AVRPlayerPawn::DecreaseLevel);

	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AVRPlayerPawn::GrabLeft()
{
	LeftHandActor->Grip(CurrentElevationLevel);
}

void AVRPlayerPawn::DropLeft()
{
	LeftHandActor->Drop();
}


void AVRPlayerPawn::GrabRight()
{
	RightHandActor->Grip(CurrentElevationLevel);
}

void AVRPlayerPawn::DropRight()
{
	UE_LOG(LogTemp, Warning, TEXT("DropRight"));
	RightHandActor->Drop();
}


void AVRPlayerPawn::PointedLeft()
{
	LeftHandActor->PointUp();
}

void AVRPlayerPawn::UnpointLeft()
{
	LeftHandActor->PointDown();
}


void AVRPlayerPawn::PointedRight()
{
	RightHandActor->PointUp();
}

void AVRPlayerPawn::UnpointRight()
{
	RightHandActor->PointDown();
}


void AVRPlayerPawn::ThumbUpLeft()
{
	LeftHandActor->ThumbUp();
}

void AVRPlayerPawn::ThumbDownLeft()
{
	LeftHandActor->ThumbDown();
}


void AVRPlayerPawn::ThumbUpRight()
{
	RightHandActor->ThumbUp();
}

void AVRPlayerPawn::ThumbDownRight()
{
	RightHandActor->ThumbDown();
}

void AVRPlayerPawn::IncreaseLevel()
{
	if (CurrentElevationLevel + 1 < 3)
		ChangeElevationLevel(CurrentElevationLevel + 1);
}

void AVRPlayerPawn::DecreaseLevel()
{
	if (CurrentElevationLevel - 1 >= 0)
		ChangeElevationLevel(CurrentElevationLevel - 1);
}

void AVRPlayerPawn::SetIntensityForEqLevelActors(TArray<AActor*> Actors, float Intensity)
{
	for (const auto Actor : Actors)
	{
		const APrehensile* Prehensile = static_cast<APrehensile*>(Actor);
		if (CurrentElevationLevel == Prehensile->ElevationLevel)
			Prehensile->LightComponent->SetIntensity(Intensity);
	}
}

void AVRPlayerPawn::ChangeElevationLevel(int NewLevel)
{
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APrehensile::StaticClass(), OutActors);
	SetIntensityForEqLevelActors(OutActors, 0);
	CurrentElevationLevel = NewLevel;
	SetIntensityForEqLevelActors(OutActors, 5000);
}
