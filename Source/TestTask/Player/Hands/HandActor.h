// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "MotionControllerComponent.h"
#include "Animation/AnimInstance.h"
#include "PlayerHandAnim.h"
#include "Materials/Material.h"
#include "Haptics/HapticFeedbackEffect_Base.h"
#include "HandActor.generated.h"

UCLASS()
class TESTTASK_API AHandActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AHandActor();
	UPROPERTY(EditAnywhere)
	USceneComponent* SceneComponent;
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* HandMesh;
	UPROPERTY(EditAnywhere)
	UCapsuleComponent* GripCapsule;
	UPROPERTY(EditAnywhere)
	USplineComponent* Teleport;
	UPROPERTY(EditAnywhere)
	TArray<USplineMeshComponent*> TeleportMeshes;
	UPROPERTY(EditAnywhere)
	UMotionControllerComponent* MotionController;
	UPROPERTY(EditAnywhere)
	UStaticMesh* TeleportStaticMesh;
	UPROPERTY(EditAnywhere)
	UStaticMesh* TeleportEndStaticMesh;
	UPROPERTY(EditAnywhere)
	UMaterial* TeleportMaterial;
	UPROPERTY(EditAnywhere)
	FRotator TeleportRotator;
	UPROPERTY(EditAnywhere)
	FVector TeleportOffset;
	UPROPERTY(EditAnywhere, Category = Haptic)
	EControllerHand HapticForHand;
	UPROPERTY(EditAnywhere, Category = Haptic)
	UHapticFeedbackEffect_Base* OverlapPrehensile;
	UStaticMeshComponent* TeleportEnd;
	TArray<AActor*> IgnoredActorsForTeleport;

protected:
	UPlayerHandAnim* Animation;
	AActor* OverlappedActor;
	// Called when the game starts or when spawned
	AActor* AttachedActor;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void ThumbUp();
	void ThumbDown();
	void Grip(int ElevationLevel);
	void Drop();
	void PointUp();
	void PointDown();
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	                    int32 BoneIndex, bool bIsSweep, const FHitResult& Hitresult);

protected:
	virtual void BeginPlay() override;
};
