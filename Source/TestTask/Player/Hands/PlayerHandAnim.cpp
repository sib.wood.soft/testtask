// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHandAnim.h"

void UPlayerHandAnim::UpdateAnimationProperties(float DeltaSeconds)
{
	NativeUpdateAnimation(DeltaSeconds);
}

void UPlayerHandAnim::NativeUpdateAnimation(float DeltaSeconds)
{
	float To = 0;
	if (bIsThumbUp) {
		To = 1;
	}
	ThumbUpBlend = ChangeValueAtRate(ThumbUpBlend, To, DeltaSeconds);

	To = 0;
	if (bIsPointing) {
		To = 1;
	}
	PointBlend = ChangeValueAtRate(PointBlend, To, DeltaSeconds);
	FlexWeight = GripAxis;
}

float UPlayerHandAnim::ChangeValueAtRate(float From, float To, float DeltaTime)
{
	float t1 = abs(To - From);
	float DeltaPerSeconds = 20;
	float dT = DeltaPerSeconds * DeltaTime;
	float RangeSign = (To - From) > 0 ? (1) : ((To - From) < 0 ? -1 : 0);
	float t2 = RangeSign * dT + From;
	if (t1 <= dT)
	{
		return To;
	}
	else {
		return t2;
	}
}