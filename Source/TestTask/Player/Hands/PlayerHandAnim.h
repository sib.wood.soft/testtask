// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PlayerHandAnim.generated.h"

/**
 *
 */
UCLASS()
class TESTTASK_API UPlayerHandAnim : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsThumbUp;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsPointing;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float FlexWeight;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float GrabWeight;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float PointBlend;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float ThumbUpBlend;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float GripAxis;
	void NativeUpdateAnimation(float DeltaSeconds) override;
	UFUNCTION(BlueprintCallable, Category = "UpdateAnimationProperties")
		void UpdateAnimationProperties(float DeltaSeconds);
protected:
	float ChangeValueAtRate(float From, float To, float DeltaTime);
};
