// Fill out your copyright notice in the Description page of Project Settings.


#include "HandActor.h"
#include "PlayerHandAnim.h"
#include "../../Interfaces/Prehensile.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/GameplayStaticsTypes.h"
#include "NavigationSystem.h"

// Sets default values
AHandActor::AHandActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	RootComponent = SceneComponent;

	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));
	MotionController->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	HandMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	HandMesh->AttachToComponent(MotionController, FAttachmentTransformRules::KeepRelativeTransform);

	GripCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("GripCapsule"));
	GripCapsule->AttachToComponent(MotionController, FAttachmentTransformRules::KeepRelativeTransform);
	GripCapsule->OnComponentBeginOverlap.AddDynamic(this, &AHandActor::OnOverlapBegin);

	Teleport = CreateDefaultSubobject<USplineComponent>("TeleportMesh");
	Teleport->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AHandActor::BeginPlay()
{
	Super::BeginPlay();
	Animation = static_cast<UPlayerHandAnim*>(HandMesh->GetAnimInstance());
}

// Called every frame
void AHandActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
/*
	Animation
*/
void AHandActor::ThumbUp()
{
	if (Animation)
	{
		Animation->bIsThumbUp = true;
	}
}

void AHandActor::ThumbDown()
{
	if (Animation)
	{
		Animation->bIsThumbUp = false;
	}
}

void AHandActor::PointUp()
{
	if (Animation)
	{
		Animation->bIsPointing = true;
	}
}

void AHandActor::PointDown()
{
	if (Animation)
	{
		Animation->bIsPointing = false;
	}
}

/*
	Grip
*/
void AHandActor::Grip(int ElevationLevel)
{
	if (Animation)
	{
		Animation->GripAxis = 1;
	}

	TArray<AActor*> OutActors;
	GetOverlappingActors(OutActors, APrehensile::StaticClass());
	if (OutActors.Num() > 0)
	{
		for (const auto OverActor : OutActors)
		{
			if (IsValid(OverActor))
			{
				static_cast<APrehensile*>(OverActor)->OnGrab();
				if (ElevationLevel==static_cast<APrehensile*>(OverActor)->ElevationLevel)
				{
					OverActor->AttachToComponent(HandMesh, FAttachmentTransformRules::KeepWorldTransform, FName("SocketGrip"));
					AttachedActor = OverActor;
					break;
				}
			}
		}
	}
}

void AHandActor::Drop()
{
	if (Animation)
	{
		Animation->GripAxis = 0;
	}
	if (IsValid(AttachedActor))
	{
		if (AttachedActor->IsA(APrehensile::StaticClass()))
		{
			AttachedActor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			static_cast<APrehensile*>(AttachedActor)->OnDrop();
		}
		AttachedActor = nullptr;
	}
}

void AHandActor::OnOverlapBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 BoneIndex, bool bIsSweep, const FHitResult& Hitresult)
{
	if (IsValid(OtherActor) && OtherActor->IsA(APrehensile::StaticClass()) && !AttachedActor)
	{
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->PlayHapticEffect(OverlapPrehensile, HapticForHand);
	}
}
